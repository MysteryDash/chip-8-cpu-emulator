﻿// © CC BY-NC-SA 4.0 - 2014 - MysteryDash
// License details can be found here : http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

using System;
using System.Collections.Generic;

namespace MysteryDash.Chip8
{
    /// <summary>
    /// Provides methods to emulate a Chip-8 CPU.
    /// </summary>
    public class Emulator
    {
        /// <summary>
        /// Emulator's Memory (4ko)
        /// </summary>
        public Byte[] Memory
        {
            get
            {
                return (Byte[])_Memory.Clone();
            }
        }
        private Byte[] _Memory { get; set; }
        /// <summary>
        /// Program Counter
        /// </summary>
        public UInt16 PC { get; private set; }
        /// <summary>
        /// CPU Registers (16 8-Bits Registers) - V0 -> VF
        /// </summary>
        public Byte[] V
        {
            get
            {
                return (Byte[])_V.Clone();
            }
        }
        private Byte[] _V { get; set; }
        /// <summary>
        /// I CPU Register (16 Bits)
        /// </summary>
        public UInt16 I { get; private set; }
        /// <summary>
        /// Emulator's CallStack
        /// </summary>
        public UInt16[] CallStack
        {
            get
            {
                return _CallStack.ToArray();
            }
        }
        private Stack<UInt16> _CallStack { get; set; }
        /// <summary>
        /// <para>The Screen Buffer (8192 Bytes)</para>
        /// <para>Screen Buffer Format : RGBA</para>
        /// <para>Resolution : 64 * 32</para>
        /// </summary>
        public Byte[] ScreenBuffer
        {
            get
            {
                return (Byte[])_ScreenBuffer.Clone();
            }
        }
        private Byte[] _ScreenBuffer { get; set; }
        /// <summary>
        /// <para>The Keyboard</para>
        /// <para>16 Booleans - One per Key</para>
        /// <para>Original Chip-8 NumPad Configuration</para>
        /// <para>1 2 3 C</para> 
        /// <para>4 5 6 D</para> 
        /// <para>7 8 9 E</para> 
        /// <para>A 0 B F</para> 
        /// </summary>
        public Boolean[] Keyboard { get; private set; }
        /// <summary>
        /// Delay Timer
        /// </summary>
        public Byte DT { get; private set; }
        /// <summary>
        /// Sound Timer
        /// </summary>
        public Byte ST { get; private set; }

        private System.Threading.Timer RefreshTimer;
        private System.Threading.Timer CPUTimer;

        /// <summary>
        /// Load a Chip-8 Rom in Memory
        /// </summary>
        /// <param name="Rom">The Rom to Load</param>
        public void Load(Byte[] Rom)
        {
            if (Rom.Length > (4096 - 512)) throw new ArgumentException("Provided Rom is too Long for the Chip-8 Emuator's Memory");

            _Memory = new Byte[4096];
            PC = 512;
            _V = new Byte[16];
            I = 0;
            _CallStack = new Stack<UInt16>();
            _ScreenBuffer = new Byte[8192];
            Keyboard = new Boolean[16];
            DT = 0;
            ST = 0;

            Byte[] Font = {
                              0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
                              0x20, 0x60, 0x20, 0x20, 0x70, // 1
                              0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
                              0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
                              0x90, 0x90, 0xF0, 0x10, 0x10, // 4
                              0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
                              0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
                              0xF0, 0x10, 0x20, 0x40, 0x40, // 7
                              0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
                              0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
                              0xF0, 0x90, 0xF0, 0x90, 0x90, // A
                              0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
                              0xF0, 0x80, 0x80, 0x80, 0xF0, // C
                              0xE0, 0x90, 0x90, 0x90, 0xE0, // D
                              0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
                              0xF0, 0x80, 0xF0, 0x80, 0x80, // F
                          };

            Font.CopyTo(_Memory, 0);
            Rom.CopyTo(_Memory, 512);
            Instruction00E0(); // Black Screen
        }

        public void Start(int CPUFrequency = 250)
        {
            RefreshTimer = new System.Threading.Timer(RefreshTimers, 0, 0, 6000 / 60);
            CPUTimer = new System.Threading.Timer(PerformNextInstruction, 0, 0, 6000 / CPUFrequency);
        }

        public void Pause()
        {
            RefreshTimer.Dispose();
            CPUTimer.Dispose();
        }

        public void Stop()
        {
            RefreshTimer.Dispose();
            CPUTimer.Dispose();

            PC = 512;
            _V = new Byte[16];
            I = 0;
            _CallStack = new Stack<UInt16>();
            _ScreenBuffer = new Byte[8192];
            Keyboard = new Boolean[16];
            DT = 0;
            ST = 0;
        }

        #region Instructions

        private void PerformNextInstruction(Object State)
        { 
            UInt16 OPCode = (UInt16)((_Memory[PC] << 8) + _Memory[PC + 1]);
            PC += 2;

            // OPCodes Available Parameters :
            // - NNN -> 0x0FFF
            // - X -> 0x0F00
            // - NN -> 0x00FF
            // - Y -> 0x00F0
            // - K -> 0x000F

            UInt16 NNN = (UInt16)(OPCode & 0x0FFF);
            Byte X = (Byte)((OPCode & 0x0F00) >> 8);
            Byte NN = (Byte)(OPCode & 0x00FF);
            Byte Y = (Byte)((OPCode & 0x00F0) >> 4);
            Byte N = (Byte)(OPCode & 0x000F);

            switch (OPCode & 0xF000)
            {
                case 0x0000:
                    switch (NN)
                    {
                        case 0xE0: Instruction00E0(); break; // Clear the display.
                        case 0xEE: Instruction00EE(); break; // Return from a subroutine.
                    }
                    break;
                case 0x1000: Instruction1NNN(NNN); break; // Jump to location nnn.
                case 0x2000: Instruction2NNN(NNN); break; // Call subroutine at nnn.
                case 0x3000: Instruction3XNN(X, NN); break; // Skip next instruction if Vx = NN.
                case 0x4000: Instruction4XNN(X, NN); break; // Skip next instruction if Vx != NN.
                case 0x5000: Instruction5XY0(X, Y); break; // Skip next instruction if Vx = Vy.
                case 0x6000: Instruction6XNN(X, NN); break; // Set Vx = NN.
                case 0x7000: Instruction7XNN(X, NN); break; // Set Vx = Vx + NN.
                case 0x8000:
                    switch (N)
                    {
                        case 0x0: Instruction8XY0(X, Y); break; // Set Vx = Vy.
                        case 0x1: Instruction8XY1(X, Y); break; // Set Vx = Vx OR Vy.
                        case 0x2: Instruction8XY2(X, Y); break; // Set Vx = Vx AND Vy.
                        case 0x3: Instruction8XY3(X, Y); break; // Set Vx = Vx XOR Vy.
                        case 0x4: Instruction8XY4(X, Y); break; // Set Vx = Vx + Vy, set VF = carry.
                        case 0x5: Instruction8XY5(X, Y); break; // Set Vx = Vx - Vy, set VF = NOT borrow.
                        case 0x6: Instruction8XY6(X, Y); break; // Set Vx = Vx SHR 1.
                        case 0x7: Instruction8XY7(X, Y); break; // Set Vx = Vy - Vx, set VF = NOT borrow.
                        case 0xE: Instruction8XYE(X, Y); break; // Set Vx = Vx SHL 1.
                    }
                    break;
                case 0x9000: Instruction9XY0(X, Y); break; // Skip next instruction if Vx != Vy.
                case 0xA000: InstructionANNN(NNN); break; // Set I = nnn.
                case 0xB000: InstructionBNNN(NNN); break; // Jump to location nnn + V0.
                case 0xC000: InstructionCXNN(X, NN); break; // Set Vx = random Byte AND NN.
                case 0xD000: InstructionDXYN(X, Y, N); break; // Display k-Byte sprite starting at memory location I at (Vx, Vy), set VF = collision.
                case 0xE000:
                    switch (NN)
                    {
                        case 0x9E: InstructionEX9E(X); break; // Skip next instruction if key with the value of Vx is pressed.
                        case 0xA1: InstructionEXA1(X); break; // Skip next instruction if key with the value of Vx is not pressed.
                    }
                    break;
                case 0xF000:
                    switch (NN)
                    {
                        case 0x07: InstructionFX07(X); break; // Set Vx = delay timer value.
                        case 0x0A: InstructionFX0A(X); break; // Wait for a key press, store the value of the key in Vx.
                        case 0x15: InstructionFX15(X); break; // Set delay timer = Vx.
                        case 0x18: InstructionFX18(X); break; // Set sound timer = Vx.
                        case 0x1E: InstructionFX1E(X); break; // Set I = I + Vx.
                        case 0x29: InstructionFX29(X); break; // Set I = location of sprite for digit Vx.
                        case 0x33: InstructionFX33(X); break; // Store BCD representation of Vx in memory locations I, I+1, and I+2.
                        case 0x55: InstructionFX55(X); break; // Store registers V0 through Vx in memory starting at location I.
                        case 0x65: InstructionFX65(X); break; // Read registers V0 through Vx from memory starting at location I.
                    }
                    break;
            }
        }

        #region Math Operations

        private void Instruction7XNN(Byte X, Byte NN)
        {
            _V[X] += NN;
        }

        private void Instruction8XY0(Byte X, Byte Y)
        {
            _V[X] = _V[Y];
        }

        private void Instruction8XY1(Byte X, Byte Y)
        {
            _V[X] |= _V[Y];
        }

        private void Instruction8XY2(Byte X, Byte Y)
        {
            _V[X] &= _V[Y];
        }

        private void Instruction8XY3(Byte X, Byte Y)
        {
            _V[X] ^= _V[Y];
        }

        private void Instruction8XY4(Byte X, Byte Y)
        {
            _V[0xF] = ((_V[X] + _V[Y]) > 0xFF) ? (Byte)1 : (Byte)0;
            _V[X] = (Byte)((_V[X] + _V[Y]) % 256);
        }

        private void Instruction8XY5(Byte X, Byte Y)
        {
            _V[0xF] = _V[X] > _V[Y] ? (Byte)1 : (Byte)0;
            _V[X] = _V[0xF] == 1 ? (Byte)(_V[X] - _V[Y]) : (Byte)0;            
        }

        private void Instruction8XY6(Byte X, Byte Y)
        {
            _V[0xF] = (Byte)(_V[X] & 0x1);
            _V[X] >>= 1;
        }

        private void Instruction8XY7(Byte X, Byte Y)
        {
            _V[0xF] = _V[Y] > _V[X] ? (Byte)1 : (Byte)0;
            _V[X] = _V[0xF] == 1 ? (Byte)(_V[Y] - _V[X]) : (Byte)0; 
        }

        private void Instruction8XYE(Byte X, Byte Y)
        { 
            _V[0xF] = (Byte)(_V[X] >> 7);
            _V[X] <<= 1;
        }

        private void InstructionFX1E(Byte X)
        {
            I += _V[X];
        }

        private void InstructionFX33(Byte X)
        {
            _Memory[I] = (byte)(_V[X] / 100);
            _Memory[I + 1] = (byte)((_V[X] % 100) / 100);
            _Memory[I + 2] = (byte)((_V[X] % 100) % 10);
        }

        #endregion

        #region Conditionnal Branching

        private void Instruction3XNN(Byte X, Byte NN)
        {
            if (_V[X] == NN)
                PC += 2;
        }

        private void Instruction4XNN(Byte X, Byte NN)
        {
            if (_V[X] != NN)
                PC += 2;
        }

        private void Instruction5XY0(Byte X, Byte Y)
        {
            if (_V[X] == _V[Y])
                PC += 2;
        }

        private void Instruction9XY0(Byte X, Byte Y)
        {
            if (_V[X] != _V[Y])
                PC += 2;
        }

        #endregion

        #region Jumps

        private void Instruction00EE()
        {
            PC = _CallStack.Pop();
        }

        private void Instruction1NNN(UInt16 NNN)
        {
            PC = NNN;
        }

        private void Instruction2NNN(UInt16 NNN)
        {
            _CallStack.Push(NNN);
            PC = NNN;
        }

        private void InstructionBNNN(UInt16 NNN)
        {
            PC = (UInt16)(NNN + _V[0x0]);
        }

        #endregion

        #region Display

        private void Instruction00E0()
        {
            for (int i = 0; i < _ScreenBuffer.Length; i += 4)
            {
                _ScreenBuffer[i] = _ScreenBuffer[i + 1] = _ScreenBuffer[i + 2] = 0x0;
                _ScreenBuffer[i + 3] = 0xFF;
            }
        }

        private void InstructionDXYN(Byte X, Byte Y, Byte N)
        {
            _V[0xF] = 0x0;

            for (Byte Line = 0; Line < N; Line++)
            {
                for (Byte BitNumber = 0; BitNumber < 8; BitNumber++)
                {
                    Byte Bit = (Byte)((Byte)(_Memory[I + Line] << BitNumber) >> 7);
                    Int32 Position = ((_V[Y] + Line ) * 64 * 4 + _V[X] + BitNumber) % _ScreenBuffer.Length;

                    if (Bit != 0 && _ScreenBuffer[Position] + _ScreenBuffer[Position + 1] + _ScreenBuffer[Position + 2] != 0)
                        _V[0xF] = 0x1;

                    if ((_ScreenBuffer[Position] + _ScreenBuffer[Position + 1] + _ScreenBuffer[Position + 2] == 0 && Bit == 0) ||
                        (_ScreenBuffer[Position] + _ScreenBuffer[Position + 1] + _ScreenBuffer[Position + 2] != 0 && Bit != 0))
                    {
                        _ScreenBuffer[Position] = _ScreenBuffer[Position + 1] = _ScreenBuffer[Position + 2] = 255;
                    }
                    else
                    {
                        _ScreenBuffer[Position] = _ScreenBuffer[Position + 1] = _ScreenBuffer[Position + 2] = 0;
                    }
                }
            }
        }

        private void InstructionFX29(Byte X)
        {
            I = (UInt16)(_V[X] * 5);
        }

        #endregion

        #region Input

        private void InstructionEX9E(Byte X)
        {
            if (Keyboard[X])
                PC += 2;
        }

        private void InstructionEXA1(Byte X)
        {
            if (!Keyboard[X])
                PC += 2;
        }

        private void InstructionFX0A(Byte X) // Blocking function
        {
            _V[X] = 0x0;
            while (_V[X] == 0)
            {
                for (byte i = 0; i < Keyboard.Length; i++)
                    _V[X] = Keyboard[i] ? i : (Byte)0;
                System.Threading.Thread.Sleep(1000 / 60); // CPU-Friendly : Check About 60 Times per Second (A Bit Less for Execution Time)
            }
        }

        #endregion

        #region Timers

        private void InstructionFX07(Byte X)
        {
            _V[X] = DT;
        }

        private void InstructionFX15(Byte X)
        {
            DT = _V[X];
        }

        private void InstructionFX18(Byte X)
        {
            ST = _V[X];
        }

        #endregion

        #region Misc

        private void Instruction6XNN(Byte X, Byte NN)
        {
            _V[X] = NN;
        }

        private void InstructionANNN(UInt16 NNN)
        {
            I = NNN;
        }

        private void InstructionCXNN(Byte X, Byte NN)
        {
            _V[X] = (Byte)(new Random().Next(256) & NN);
        }

        private void InstructionFX55(Byte X)
        {
            for (byte i = 0; i <= X; i++)
                _Memory[I + i] = _V[i];
        }

        private void InstructionFX65(Byte X)
        {
            for (byte i = 0; i <= X; i++)
                _V[i] = _Memory[I + i];
        }

        #endregion

        #endregion

        private void RefreshTimers(Object State)
        {
            if (DT > 0) DT--;
            if (ST > 0) ST--;
        }
    }
}